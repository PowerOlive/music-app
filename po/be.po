# Belarusian translation for music-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the music-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: music-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-27 19:11-0600\n"
"PO-Revision-Date: 2018-12-28 09:08+0000\n"
"Last-Translator: ierihon <idekovets@mail.ru>\n"
"Language-Team: Belarusian <https://translate.ubports.com/projects/ubports/"
"music-app/be/>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.1.1\n"
"X-Launchpad-Export-Date: 2017-04-14 05:53+0000\n"

#: ../app/components/Dialog/ContentHubErrorDialog.qml:35
msgid "OK"
msgstr "ОК"

#: ../app/components/Dialog/ContentHubNotFoundDialog.qml:29
msgid "Imported file not found"
msgstr "Імпартаваны файл не знойдзены"

#: ../app/components/Dialog/ContentHubNotFoundDialog.qml:33
msgid "Wait"
msgstr "Чакайце"

#: ../app/components/Dialog/ContentHubNotFoundDialog.qml:43
#: ../app/components/Dialog/EditPlaylistDialog.qml:76
#: ../app/components/Dialog/NewPlaylistDialog.qml:71
#: ../app/components/Dialog/RemovePlaylistDialog.qml:60
msgid "Cancel"
msgstr "Скасаваць"

#: ../app/components/Dialog/ContentHubWaitDialog.qml:34
msgid "Waiting for file(s)..."
msgstr "Чаканне файла(-ў)…"

#. TRANSLATORS: this is a title of a dialog with a prompt to rename a playlist
#: ../app/components/Dialog/EditPlaylistDialog.qml:30
msgid "Rename playlist"
msgstr "Пераназваць спіс прайгравання"

#: ../app/components/Dialog/EditPlaylistDialog.qml:37
#: ../app/components/Dialog/NewPlaylistDialog.qml:34
msgid "Enter playlist name"
msgstr "Увядзіце назву ліста"

#: ../app/components/Dialog/EditPlaylistDialog.qml:46
msgid "Change"
msgstr "Змяніць"

#: ../app/components/Dialog/EditPlaylistDialog.qml:67
#: ../app/components/Dialog/NewPlaylistDialog.qml:60
msgid "Playlist already exists"
msgstr "Плэйліст ўжо існуе"

#: ../app/components/Dialog/EditPlaylistDialog.qml:71
#: ../app/components/Dialog/NewPlaylistDialog.qml:65
msgid "Please type in a name."
msgstr "Калі ласка, увядзіце імя."

#: ../app/components/Dialog/NewPlaylistDialog.qml:30
msgid "New playlist"
msgstr "Новы спіс прайгравання"

#: ../app/components/Dialog/NewPlaylistDialog.qml:44
msgid "Create"
msgstr "Стварыць"

#. TRANSLATORS: this is a title of a dialog with a prompt to delete a playlist
#: ../app/components/Dialog/RemovePlaylistDialog.qml:31
msgid "Permanently delete playlist?"
msgstr "Канчаткова выдаліць спіс прайгравання?"

#: ../app/components/Dialog/RemovePlaylistDialog.qml:32
msgid "This cannot be undone"
msgstr "Наступствы дзеяння нельга будзе адмяніць"

#: ../app/components/Dialog/RemovePlaylistDialog.qml:37
#: ../app/components/ListItemActions/Remove.qml:27
msgid "Remove"
msgstr "Выдаліць"

#: ../app/components/HeadState/MultiSelectHeadState.qml:28
msgid "Select All"
msgstr "Абраць усе"

#. TRANSLATORS: this action appears in the overflow drawer with limited space (around 18 characters)
#: ../app/components/HeadState/MultiSelectHeadState.qml:40
#: ../app/components/ListItemActions/AddToPlaylist.qml:26
#: ../app/ui/NowPlaying.qml:105
msgid "Add to playlist"
msgstr "Дадаць у плэйліст"

#: ../app/components/HeadState/MultiSelectHeadState.qml:58
msgid "Add to queue"
msgstr "Дадаць у чаргу"

#: ../app/components/HeadState/MultiSelectHeadState.qml:77
msgid "Delete"
msgstr "Выдаліць"

#: ../app/components/HeadState/MultiSelectHeadState.qml:89
msgid "Cancel selection"
msgstr "Скасаваць выбар"

#: ../app/components/HeadState/SearchHeadState.qml:42
msgid "Search music"
msgstr "Пошук музыкі"

#. TRANSLATORS: This string represents that the target destination filepath does not start with ~/Music/Imported/
#: ../app/components/Helpers/ContentHubHelper.qml:86
msgid "Filepath must start with"
msgstr "Шлях да файла мусіць пачынацца з"

#. TRANSLATORS: This string represents that a blank filepath destination has been used
#: ../app/components/Helpers/ContentHubHelper.qml:112
msgid "Filepath must be a file"
msgstr "Шлях да файла мусіць быць файлам"

#. TRANSLATORS: This string represents that there was failure moving the file to the target destination
#: ../app/components/Helpers/ContentHubHelper.qml:118
msgid "Failed to move file"
msgstr "Не атрымалася перанесці файл"

#. TRANSLATORS: this refers to a number of tracks greater than one. The actual number will be prepended to the string automatically (plural forms are not yet fully supported in usermetrics, the library that displays that string)
#: ../app/components/Helpers/UserMetricsHelper.qml:31
msgid "tracks played today"
msgstr "трэкаў праслухана сёння"

#: ../app/components/Helpers/UserMetricsHelper.qml:32
msgid "No tracks played today"
msgstr "Няма праслуханых трэкаў сёння"

#: ../app/components/ListItemActions/AddToQueue.qml:28
msgid "Add to Queue"
msgstr "Дадаць у чаргу"

#: ../app/components/LoadingSpinnerComponent.qml:47
msgid "Loading..."
msgstr "Загрузка..."

#: ../app/components/MusicPage.qml:42
msgid "No items found"
msgstr "Нічога не знойдзена"

#: ../app/components/MusicToolbar.qml:91
msgid "Tap to shuffle music"
msgstr "Націсніце, каб змяшаць музыку"

#: ../app/components/PlaylistsEmptyState.qml:42
msgid "No playlists found"
msgstr "Спісы не знойдзеныя"

#: ../app/components/PlaylistsEmptyState.qml:53
#, qt-format
msgid ""
"Get more out of Music by tapping the %1 icon to start making playlists for "
"every mood and occasion."
msgstr ""
"Атрымаеце як мага больш ад «Музыкі»: значок %1, каб пачаць стварэнне спісаў "
"прайгравання для любога настрою і нагоды."

#. TRANSLATORS: this appears in a button with limited space (around 14 characters)
#: ../app/components/ViewButton/PlayAllButton.qml:28
msgid "Play all"
msgstr "Граць усё"

#. TRANSLATORS: this appears in a button with limited space (around 14 characters)
#: ../app/components/ViewButton/QueueAllButton.qml:46
msgid "Queue all"
msgstr "Выбраць усе"

#. TRANSLATORS: this appears in a button with limited space (around 14 characters)
#: ../app/components/ViewButton/ShuffleButton.qml:46
msgid "Shuffle"
msgstr "Выпадкова"

#: ../app/components/Walkthrough/Slide1.qml:59
msgid "Welcome to Music"
msgstr "Вітаем у \"Музыцы\""

#: ../app/components/Walkthrough/Slide1.qml:73
msgid ""
"Enjoy your favorite music with Ubuntu's Music App. Take a short tour on how "
"to get started or press skip to start listening now."
msgstr ""
"Атрымлівайце задавальненне ад любімай музыкі з дапамогай праграмы «Музыка» "
"Ubuntu. Азнаёмцеся з пачатковымі даведачнымі дадзенымі або націсніце кнопку "
"«Прапусціць», каб перайсці да праслухоўвання."

#: ../app/components/Walkthrough/Slide2.qml:54
msgid "Import your music"
msgstr "Імпартаваць вашу музыку"

#: ../app/components/Walkthrough/Slide2.qml:67
#: ../app/ui/LibraryEmptyState.qml:131
msgid ""
"Connect your device to any computer and simply drag files to the Music "
"folder or insert removable media with music."
msgstr ""
"Злучыце прыладу з любым кампутарам і проста перацягніце файлы ў тэчку музыкі "
"або устаўце партатыўны носьбіт з музычнымі запісамі."

#: ../app/components/Walkthrough/Slide3.qml:54
msgid "Download new music"
msgstr "Атрымаць новую музыку"

#: ../app/components/Walkthrough/Slide3.qml:67
msgid "Directly import music bought while browsing online."
msgstr ""
"Непасрэдна імпартаваць музычныя творы, набытыя падчас падарожжаў у інтэрнэце."

#: ../app/components/Walkthrough/Slide3.qml:81
msgid "Start"
msgstr "Пуск"

#: ../app/components/Walkthrough/Walkthrough.qml:119
msgid "Skip"
msgstr "Мінуць"

#: ../app/music-app.qml:158
msgid "Next"
msgstr "Далей"

#: ../app/music-app.qml:159
msgid "Next Track"
msgstr "Наступная кампазіцыя"

#: ../app/music-app.qml:165
msgid "Pause"
msgstr "Прыпыніць"

#: ../app/music-app.qml:165
msgid "Play"
msgstr "Прайграць"

#: ../app/music-app.qml:167
msgid "Pause Playback"
msgstr "Прыпыніць прайграванне"

#: ../app/music-app.qml:167
msgid "Continue or start playback"
msgstr "Працягнуць або пачаць прайграванне"

#: ../app/music-app.qml:172
msgid "Back"
msgstr "Назад"

#: ../app/music-app.qml:173
msgid "Go back to last page"
msgstr "Вярнуцца на апошнюю старонку"

#: ../app/music-app.qml:181
msgid "Previous"
msgstr "Назад"

#: ../app/music-app.qml:182
msgid "Previous Track"
msgstr "Папярэдняя кампазіцыя"

#: ../app/music-app.qml:187
msgid "Stop"
msgstr "Спыніць"

#: ../app/music-app.qml:188
msgid "Stop Playback"
msgstr "Спыніць прайграванне"

#: ../app/music-app.qml:273 com.ubuntu.music_music.desktop.in.in.h:1
msgid "Music"
msgstr "Музыка"

#: ../app/music-app.qml:303
msgid "Debug: "
msgstr "Наладка: "

#. TRANSLATORS: this appears in the header with limited space (around 20 characters)
#: ../app/music-app.qml:848 ../app/ui/NowPlaying.qml:38
msgid "Now playing"
msgstr "Зараз грае"

#. TRANSLATORS: this appears in the header with limited space (around 20 characters)
#: ../app/ui/AddToPlaylist.qml:43
msgid "Select playlist"
msgstr "Выбраць плэйліст"

#: ../app/ui/AddToPlaylist.qml:99 ../app/ui/Playlists.qml:94
#: ../app/ui/SongsView.qml:291 ../app/ui/SongsView.qml:292
#, qt-format
msgid "%1 track"
msgid_plural "%1 tracks"
msgstr[0] "%1 трэк"
msgstr[1] "%1 трэкаў"
msgstr[2] "%1 трэка"

#. TRANSLATORS: this is the name of the playlists page shown in the tab header.
#. Remember to keep the translation short to fit the screen width
#: ../app/ui/AddToPlaylist.qml:104 ../app/ui/Playlists.qml:36
#: ../app/ui/SongsView.qml:102
msgid "Playlists"
msgstr "Спісы прайгравання"

#: ../app/ui/AddToPlaylist.qml:114 ../app/ui/Recent.qml:34
#: ../app/ui/SongsView.qml:90
msgid "Recent"
msgstr "Нядаўнія"

#: ../app/ui/Albums.qml:32
msgid "Albums"
msgstr "Альбомы"

#: ../app/ui/Albums.qml:82 ../app/ui/ArtistView.qml:136
#: ../app/ui/ArtistView.qml:149 ../app/ui/Recent.qml:88
#: ../app/ui/SongsView.qml:248
msgid "Unknown Album"
msgstr "Невядомы альбом"

#: ../app/ui/Albums.qml:83 ../app/ui/ArtistView.qml:75
#: ../app/ui/ArtistView.qml:148 ../app/ui/Artists.qml:86
#: ../app/ui/Recent.qml:89 ../app/ui/SongsView.qml:269
msgid "Unknown Artist"
msgstr "Невядомы выканаўца"

#: ../app/ui/Albums.qml:93 ../app/ui/ArtistView.qml:147
#: ../app/ui/Recent.qml:104
msgid "Album"
msgstr "Альбом"

#: ../app/ui/ArtistView.qml:94
#, qt-format
msgid "%1 album"
msgid_plural "%1 albums"
msgstr[0] "%1 Альбом"
msgstr[1] "%1 Альбома"
msgstr[2] "%1 Альбомаў"

#: ../app/ui/Artists.qml:36
msgid "Artists"
msgstr "Выканаўцы"

#: ../app/ui/Artists.qml:94
msgid "Artist"
msgstr "Выканаўца"

#: ../app/ui/ContentHubExport.qml:34
msgid "Export Track"
msgstr "Экспартаваць трэк"

#: ../app/ui/Genres.qml:32
msgid "Genres"
msgstr "Жанры"

#: ../app/ui/Genres.qml:118 ../app/ui/Genres.qml:120
#: ../app/ui/SongsView.qml:200 ../app/ui/SongsView.qml:219
#: ../app/ui/SongsView.qml:234 ../app/ui/SongsView.qml:271
#: ../app/ui/SongsView.qml:290 ../app/ui/SongsView.qml:317
msgid "Genre"
msgstr "Жанр"

#: ../app/ui/LibraryEmptyState.qml:119
msgid "No music found"
msgstr "Музыка не знойдзеная"

#. TRANSLATORS: this appears in the header with limited space (around 20 characters)
#: ../app/ui/NowPlaying.qml:40
msgid "Full view"
msgstr "Поўны прагляд"

#. TRANSLATORS: this appears in the header with limited space (around 20 characters)
#: ../app/ui/NowPlaying.qml:42
msgid "Queue"
msgstr "Чарга"

#. TRANSLATORS: this action appears in the overflow drawer with limited space (around 18 characters)
#: ../app/ui/NowPlaying.qml:122
msgid "Clear queue"
msgstr "Ачысціць чаргу"

#: ../app/ui/Playlists.qml:106 ../app/ui/Playlists.qml:107
#: ../app/ui/Recent.qml:89 ../app/ui/Recent.qml:104 ../app/ui/SongsView.qml:66
#: ../app/ui/SongsView.qml:81 ../app/ui/SongsView.qml:113
#: ../app/ui/SongsView.qml:153 ../app/ui/SongsView.qml:187
#: ../app/ui/SongsView.qml:202 ../app/ui/SongsView.qml:221
#: ../app/ui/SongsView.qml:233 ../app/ui/SongsView.qml:270
#: ../app/ui/SongsView.qml:300 ../app/ui/SongsView.qml:303
#: ../app/ui/SongsView.qml:319
msgid "Playlist"
msgstr "Спіс прайгравання"

#: ../app/ui/Songs.qml:36
msgid "Tracks"
msgstr "Трэкі"

#: com.ubuntu.music_music.desktop.in.in.h:2
msgid "A music application for Ubuntu"
msgstr "Музычнае прыкладанне для Ubuntu"

#: com.ubuntu.music_music.desktop.in.in.h:3
msgid "music;songs;play;tracks;player;tunes;"
msgstr "музыка; песні; граць; трэк; прайгравач; мелодыі;"
